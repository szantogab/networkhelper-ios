//
//  JSONParser.m
//  NetworkHelper
//
//  Created by Szanto Gabor on 20/03/14.
//  Copyright (c) 2014 Szanto Gabor. All rights reserved.
//

#import "JSONParser.h"

@implementation JSONParser

+(void)parseUrl:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(JSONParserSuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock
{
    [HTTPManager sendRequest:httpMethod url:url headers:headers bodyParams:params cachePolicy:cachePolicy successBlock:^(NSData *response, NSInteger statusCode)
     {
         NSError* error = nil;
         
         NSDictionary* jsonDictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
         
         if (!error && jsonDictionary && [jsonDictionary isKindOfClass:[NSDictionary class]])
         {
             if (successBlock) successBlock(jsonDictionary, statusCode);
         }
         else
         {
             if (failedBlock) failedBlock(error);
         }
         
     } failedBlock:^(NSError *error)
     {
         if (failedBlock) failedBlock(error);
     }];
}

+(void)parseUrl:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params successBlock:(JSONParserSuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock
{
    [self parseUrl:httpMethod url:url headers:headers bodyParams:params cachePolicy:NSURLRequestReloadIgnoringCacheData successBlock:successBlock failedBlock:failedBlock];
}

+(void)parseUrl:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers body:(NSString*)body cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(JSONParserSuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock
{
    [HTTPManager sendRequest:httpMethod url:url headers:headers body:body cachePolicy:cachePolicy successBlock:^(NSData *response, NSInteger statusCode)
     {
         NSError* error = nil;
         
         NSDictionary* jsonDictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
         
         if (!error && jsonDictionary && [jsonDictionary isKindOfClass:[NSDictionary class]])
         {
             if (successBlock) successBlock(jsonDictionary, statusCode);
         }
         else
         {
             if (failedBlock) failedBlock(error);
         }
         
     } failedBlock:^(NSError *error)
     {
         if (failedBlock) failedBlock(error);
     }];
}


+(void)parseUrlArray:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(JSONParserArraySuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock
{
    [HTTPManager sendRequest:httpMethod url:url headers:headers bodyParams:params cachePolicy:cachePolicy successBlock:^(NSData *response, NSInteger statusCode)
     {
         NSError* error = nil;
         
         NSArray* jsonArray = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
         
         if (!error)
         {
             if (successBlock) successBlock(jsonArray, statusCode);
         }
         else
         {
             if (failedBlock) failedBlock(error);
         }
         
     } failedBlock:^(NSError *error)
     {
         if (failedBlock) failedBlock(error);
     }];
}

+(void)parseUrlArray:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params successBlock:(JSONParserArraySuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock
{
    [self parseUrlArray:httpMethod url:url headers:headers bodyParams:params cachePolicy:NSURLRequestReloadIgnoringCacheData successBlock:successBlock failedBlock:failedBlock];
}

+(void)parseUrlArray:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers body:(NSString*)body cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(JSONParserArraySuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock
{
    [HTTPManager sendRequest:httpMethod url:url headers:headers body:body cachePolicy:cachePolicy successBlock:^(NSData *response, NSInteger statusCode)
     {
         NSError* error = nil;
         
         NSArray* jsonArray = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
         
         if (!error)
         {
             if (successBlock) successBlock(jsonArray, statusCode);
         }
         else
         {
             if (failedBlock) failedBlock(error);
         }
         
     } failedBlock:^(NSError *error)
     {
         if (failedBlock) failedBlock(error);
     }];
}


@end
