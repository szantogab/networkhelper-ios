//
//  HTTPManager.h
//  HTTPManager
//
//  Created by Szanto Gabor on 10/02/14.
//  Copyright (c) 2014 Szanto Gabor. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TIMEOUT_SECONDS 30

typedef void (^HTTPManagerSuccessBlock)(NSData* data, NSInteger statusCode);
typedef void (^HTTPManagerFailBlock)(NSError* error);

typedef enum {
    GET = 0,
    POST = 1,
    PUT = 2,
    PATCH = 3,
    DELETE = 4,
    HEAD = 5,
    OPTIONS = 6
} HttpMethod;

@interface HTTPRequest : NSObject
{
    NSURLRequest* request;
    HTTPManagerSuccessBlock successBlock;
    HTTPManagerFailBlock failBlock;
}

-(id)initWithRequest:(NSURLRequest*)_request successBlock:(HTTPManagerSuccessBlock)_successBlock failBlock:(HTTPManagerFailBlock)_failBlock;

@property (nonatomic) NSURLRequest* request;
@property (readonly, nonatomic) HTTPManagerSuccessBlock successBlock;
@property (readonly, nonatomic) HTTPManagerFailBlock failBlock;

@end

@interface HTTPManager : NSObject<NSURLConnectionDelegate>
{
}

+(void)sendRequest:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(HTTPManagerSuccessBlock)successBlock failedBlock:(HTTPManagerFailBlock)failedBlock;

+(void)sendRequest:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers body:(NSString*)body cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(HTTPManagerSuccessBlock)successBlock failedBlock:(HTTPManagerFailBlock)failedBlock;

+(void)setDefaultHost:(NSString*)host;

+(NSString*)urlEncodeString:(NSString*)unencodedString;

@end
