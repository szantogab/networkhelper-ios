//
//  NetworkHelper.m
//  NetworkHelper
//
//  Created by Szanto Gabor on 10/02/14.
//  Copyright (c) 2014 Szanto Gabor. All rights reserved.
//

#import "HTTPManager.h"

@implementation HTTPRequest

@synthesize request;
@synthesize successBlock;
@synthesize failBlock;

static NSString* defaultHost = nil;

-(id)initWithRequest:(NSURLRequest*)_request successBlock:(HTTPManagerSuccessBlock)_successBlock failBlock:(HTTPManagerFailBlock)_failBlock
{
    if (self = [super init])
    {
        request = _request;
        successBlock = _successBlock;
        failBlock = _failBlock;
    }
    
    return self;
}

@end

@implementation HTTPManager

+(void)sendRequest:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(HTTPManagerSuccessBlock)successBlock failedBlock:(HTTPManagerFailBlock)failedBlock
{
    [HTTPManager sendRequest:httpMethod url:url headers:headers bodyParams:params body:nil cachePolicy:cachePolicy successBlock:successBlock failedBlock:failedBlock];
}

+(void)sendRequest:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers body:(NSString*)body cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(HTTPManagerSuccessBlock)successBlock failedBlock:(HTTPManagerFailBlock)failedBlock
{
    [HTTPManager sendRequest:httpMethod url:url headers:headers bodyParams:nil body:body cachePolicy:cachePolicy successBlock:successBlock failedBlock:failedBlock];
}

+(void)sendRequest:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params body:(NSString*)body cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(HTTPManagerSuccessBlock)successBlock failedBlock:(HTTPManagerFailBlock)failedBlock
{
    if (![url hasPrefix:@"http"] && defaultHost)
    {
        if ([defaultHost hasSuffix:@"/"]) defaultHost = [defaultHost substringToIndex:[defaultHost length]-1];
        if ([url hasPrefix:@"/"]) url = [url substringFromIndex:1];

        url = [NSString stringWithFormat:@"%@/%@", defaultHost, url];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:cachePolicy timeoutInterval:TIMEOUT_SECONDS];
    request.HTTPMethod = [HTTPManager formatTypeToString:httpMethod];
    
    // Set the header fields, if any
    if (headers)
    {
        for (NSString* key in headers)
        {
            NSString* value = [headers objectForKey:key];
            [request setValue:value forHTTPHeaderField:key];
        }
    }
    
    // Set the body params
    if (params)
    {
        [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];

        NSString* payload = @"";
        
        for (NSString* key in params)
        {
            NSString* value = [params objectForKey:key];
            if (value)
            {
                if (payload.length != 0) payload = [payload stringByAppendingString:@"&"];
                payload = [payload stringByAppendingFormat:@"%@=%@", key, [self urlEncodeString:value]];
            }
        }
        
        NSData *requestBodyData = [payload dataUsingEncoding:NSUTF8StringEncoding];
        request.HTTPBody = requestBodyData;
    }
    else if (body)
    {
        request.HTTPBody = [body dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    HTTPRequest* _request = [[HTTPRequest alloc] initWithRequest:request successBlock:successBlock failBlock:failedBlock];
    [self startHTTPRequest:_request];
}

+(void)setDefaultHost:(NSString*)host
{
    defaultHost = host;
}

+(void)startHTTPRequest:(HTTPRequest*)request
{
    [NSURLConnection sendAsynchronousRequest:request.request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if (!connectionError || data)
         {
             if (request.successBlock)
             {
                 NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
                 request.successBlock(data, httpResponse.statusCode);
             }
         }
         else
         {
             if (request.failBlock)
             {
                 request.failBlock(connectionError);
             }
             
         }
     }];
}

+(NSString*)formatTypeToString:(HttpMethod)httpMethod
{
    NSString *result = nil;
    
    switch(httpMethod) {
        case GET:
            result = @"GET";
            break;
        case POST:
            result = @"POST";
            break;
        case PUT:
            result = @"PUT";
            break;
        case PATCH:
            result = @"PATCH";
            break;
        case DELETE:
            result = @"DELETE";
            break;
        case HEAD:
            result = @"HEAD";
            break;
        case OPTIONS:
            result = @"OPTIONS";
            break;
    }
    
    return result;
}

+(NSString*)urlEncodeString:(NSString*)unencodedString
{
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)unencodedString, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    
    return encodedString;
}

@end
