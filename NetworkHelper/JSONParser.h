//
//  JSONParser.h
//  NetworkHelper
//
//  Created by Szanto Gabor on 20/03/14.
//  Copyright (c) 2014 Szanto Gabor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTTPManager.h"

typedef void (^JSONParserSuccessBlock)(NSDictionary* result, NSInteger statusCode);
typedef void (^JSONParserArraySuccessBlock)(NSArray* result, NSInteger statusCode);
typedef void (^JSONParserFailBlock)(NSError* error);


@interface JSONParser : NSObject

+(void)parseUrl:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(JSONParserSuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock;

+(void)parseUrl:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params successBlock:(JSONParserSuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock;

+(void)parseUrl:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers body:(NSString*)body cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(JSONParserSuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock;

+(void)parseUrlArray:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(JSONParserArraySuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock;

+(void)parseUrlArray:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers bodyParams:(NSDictionary*)params successBlock:(JSONParserArraySuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock;

+(void)parseUrlArray:(HttpMethod)httpMethod url:(NSString*)url headers:(NSDictionary*)headers body:(NSString*)body cachePolicy:(NSURLRequestCachePolicy)cachePolicy successBlock:(JSONParserArraySuccessBlock)successBlock failedBlock:(JSONParserFailBlock)failedBlock;



@end
