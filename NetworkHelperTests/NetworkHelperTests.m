//
//  NetworkHelperTests.m
//  NetworkHelperTests
//
//  Created by Szanto Gabor on 10/02/14.
//  Copyright (c) 2014 Szanto Gabor. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HTTPManager.h"

@interface NetworkHelperTests : XCTestCase

@end

@implementation NetworkHelperTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    [HTTPManager sendRequest:POST url:@"http://graph.facebook.com/szantogab" headers:nil bodyParams:nil successBlock:^(NSString *response, NSInteger statusCode)
    {
        NSLog(@"Response: %@", response);
        XCTAssertTrue (response != nil, @"Response: %@", response);
    } failedBlock:^(NSError *error) {

    }];

    //XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

-(void)httpTest
{
}

@end
